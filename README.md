CV
==

This is my Curriculum Vitae template in php

If you want to use it for your own CV, it's totally fine.

You just need to replace in fr/config.php AND en/config.php the different content of your CV.
You could also add/delete categories quite easily by following the structure already existing :)
enjoy!


You can find the online version of it at https://pipit.u-strasbg.fr/~brugneaux/
