<?php
$lang['title'] = $personal['first_name'] . ' ' . $personal['last_name'] . " : CV en ligne";
$lang['header'] = $personal['first_name'] . ' ' . $personal['last_name'];
$lang['print'] = "<img src=\"../images/print.png\" alt>";
$lang['contact'] = "Contact";
$lang['contact_desc'] = "";
$lang['phone'] = "Téléphone";
$lang['email'] = "Email";
$lang['education'] = "Parcours scolaire";
$lang['skills'] = "Compétences";
$lang['awards'] = "Récompenses";
$lang['interests'] = 'Centres d\'intérêt & hobbies';
$lang['address'] = "Adresses actuelles";
$lang['social_media'] = "Réseaux sociaux";
$lang['career'] = "Parcours professionel";
$lang['pdf'] = "Téléchargez version PDF";
?>