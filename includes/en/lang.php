<?php
$lang['title'] = $personal['first_name'] . ' ' . $personal['last_name'] . " : CV Online";
$lang['header'] = $personal['first_name'] . ' ' . $personal['last_name'];
$lang['print'] = "<img src=\"../images/print.png\" alt>" /*Print Me */;
$lang['contact'] = "Contact";
$lang['contact_desc'] = "";//"I'm an international student currently living in Denmark.<br/>These are the different ways to speak with me.";
$lang['phone'] = "Phone";
$lang['email'] = "Email";
$lang['education'] = "Education";
$lang['skills'] = "Skills";
$lang['awards'] = "Awards &amp; Honors";
$lang['interests'] = "Interests &amp; Hobbies";
$lang['address'] = "Current addresses";
$lang['social_media'] = "Social Networks";
$lang['career'] = "Career";
$lang['pdf'] = "Download PDF version";
?>